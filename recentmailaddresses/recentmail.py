uniqueemailaddresses = {}


def load(filename):
    try:
        with open(filename, "r") as f:
            emailaddress = None
            for line in f:
                line = line.rstrip()
                if len(line) < 2:
                    continue
                if line[0] != " ":
                    if emailaddress and not emailaddress in uniqueemailaddresses:
                        uniqueemailaddresses[emailaddress] = set()
                    emailaddress = line
                else:
                    fullname = line.lstrip()
                    if fullname:
                        uniqueemailaddresses.setdefault(
                            emailaddress, set()).add(fullname)
    except FileNotFoundError:
        pass  # Ignore if file does not exist


def save(filename):
    with open(filename, "w") as f:
        for emailaddress in sorted(uniqueemailaddresses):
            print(emailaddress, file=f)
            for fullname in sorted(uniqueemailaddresses[emailaddress]):
                if fullname.lower() != emailaddress:
                    print("  ", fullname, file=f)

#!/usr/bin/env python3

import sys
import os

import recentmail

recentmail.load(
    "/tmp/.{username}.recentemailaddresses".format(username=os.getlogin()))

os.system(
    "khard email --parsable '{}'".format(sys.argv[-1]))

needle = sys.argv[-1].lower()

for emailaddress in sorted(recentmail.uniqueemailaddresses):
    printedsomething = False
    for fullname in sorted(recentmail.uniqueemailaddresses[emailaddress]):
        if fullname:
            printedsomething = True
            if needle in emailaddress.lower() or needle in fullname.lower():
                print(emailaddress, fullname, "recent", sep="\t")
    if not printedsomething:
        fullname = emailaddress.split('@')[0]
        if needle in emailaddress.lower():
            print(emailaddress, fullname, "recent", sep="\t")

/* Copyright (c) 2015, Thomas Fischer <fischer@unix-ag.uni-kl.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Thomas Fischer nor the names of other contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 8192

#define NO_EXIT      -1
#define EXIT_ALL_OK   0
#define EXIT_ERROR    1

int process_cmd_line_arguments(int argc, char *argv[]) {
    for (int i = 1; i < argc; ++i) {
        if (argv[ i ][ 0 ] == '-') {
            if ((argv[ i ][ 1 ] == 'v' && argv[ i ][ 2 ] == 0) || strcmp(argv[ i ], "--version") == 0) {
                fprintf(stderr, "Version 20151220\n\n");
                return EXIT_ALL_OK;
            } else if ((argv[ i ][ 1 ] == 'h' && argv[ i ][ 2 ] == 0) || strcmp(argv[ i ], "--help") == 0) {
                fprintf(stderr, "Copyright (c) 2015 Thomas Fischer.\nAll rights reserved.\n\n");
                fprintf(stderr, "Get the source code at:\n  https://gitorious.org/tfscripts/linuxcommandline\nReleased under the three-clause BSD license\n\n");
                fprintf(stderr, "Reads lines from stdin and prints them 0-byte separated.\n\n");
                return EXIT_ALL_OK;
            }
        }
    }
    return NO_EXIT;
}

int main(int argc, char *argv[]) {
    /// Process command line arguments if any given
    switch (process_cmd_line_arguments(argc, argv)) {
    case EXIT_ALL_OK: return 0;
    case EXIT_ERROR: return 1;
    case NO_EXIT: /** continue program */ break;
    }

    char line[BUFFER_SIZE];

    while (!feof(stdin) && fgets(line, BUFFER_SIZE, stdin) != NULL) {
        /// Remove non-printable ASCII characters (like \r or \n) at lines' end
        for (int i = strlen(line) - 1; i >= 0 && line[i] < 32; --i)
            line[i] = '\0';

        /// Print string excluding terminating \0; no 'printf' required as no formatting desired
        fputs(line, stdout);
        /// Print out \0
        fputc(0, stdout);

        /// Flush after each line printed
        fflush(stdout);
    }

    /// Never complain, always succeed
    return 0;
}

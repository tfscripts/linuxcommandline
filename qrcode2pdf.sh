#!/usr/bin/env bash

TEMPNAME=$(mktemp)

function svgtopdf() {
	local svgfilename="$1"
	local base="$2"
	rsvg-convert -f pdf -o "${base}-with-margin.pdf" "${svgfilename}" && pdfcrop --margins "0 0 0 0" "${base}-with-margin.pdf" "${base}.pdf"
	ls -l "${base}.pdf" "${base}-with-margin.pdf"
	test -s "${base}.pdf" || { rm -f "${base}-with-margin.pdf" "${base}.pdf" ; exit 1 ; }
}

function epstopdf() {
	local epsfilename="$1"
	local base="$2"
	/usr/bin/epstopdf "--outfile=${base}-with-margin.pdf" "${epsfilename}" && pdfcrop --margins "0 0 0 0" "${base}-with-margin.pdf" "${base}.pdf"
	ls -l "${base}.pdf" "${base}-with-margin.pdf"
	test -s "${base}.pdf" || { rm -f "${base}-with-margin.pdf" "${base}.pdf" ; exit 1 ; }
}

if which svgtopdf 2>/dev/null >&2 ; then
	TYPE=SVG
	TOPDFFUNCTION=svgtopdf
elif which epstopdf 2>/dev/null >&2 ; then
	TYPE=EPS
	TOPDFFUNCTION=epstopdf
else
	TYPE=PNG
	TOPDFFUNCTION=/bin/false
fi

if [[ "$1" = "--help" || "$1" = "-h" ]] ; then
	echo "There are the following ways to run this script:" >&2
	echo "1. No arguments: text will be read from stdin," >&2
	echo "   the resulting QR code PDF file gets a random filename" >&2
	echo "2. Filenames as arguments: text will be read from file," >&2
	echo "   the resulting QR code PDF file gets named after input file" >&2
	echo "3. Command line arguments pass to this script used as text," >&2
	echo "   the resulting QR code PDF file gets named after text" >&2
elif [[ $# -eq 0 ]] ; then
	# no arguments provided, reading from stdin
	echo "Hint: reading from stdin, used argument --help to get help" >&2
	base="qrcode_stdin_"$(date '+%Y%m%d-%H%M')"-"${RANDOM}
	qrencode -o "${TEMPNAME}" -t ${TYPE} </dev/stdin || { rm -f "${TEMPNAME}" ; exit 1 ; }
	${TOPDFFUNCTION} "${TEMPNAME}" "${base}" || { rm -f "${TEMPNAME}" ; exit 1 ; }
	rm -f "${TEMPNAME}"
else
	for arg in "$@" ; do
		if [[ -f "${arg}" ]] ; then
			# argument is an existing file
			base=$(sed -e 's/[^a-zA-Z0-9_-]/_/g;s/___*/_/g' <<<"${arg}" | tr '[A-Z]' '[a-z]')
			base="qrcode_file_${base}"
			qrencode -o "${TEMPNAME}" -t ${TYPE} <"${arg}" || { rm -f "${TEMPNAME}" ; exit 1 ; }
		else
			# argument is any string
			base=$(sed -e 's/[^a-zA-Z0-9_-]/_/g;s/___*/_/g' <<<"${arg}" | tr '[A-Z]' '[a-z]')
			base="qrcode_${base}"
			qrencode -o "${TEMPNAME}" -t ${TYPE} "${arg}" || { rm -f "${TEMPNAME}" ; exit 1 ; }
		fi
		${TOPDFFUNCTION} "${TEMPNAME}" "${base}" || { rm -f "${TEMPNAME}" ; exit 1 ; }
		rm -f "${TEMPNAME}"
	done
fi

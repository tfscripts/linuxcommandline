#!/usr/bin/env bash

#  Copyright (c) 2011-2018, Thomas Fischer <fischer@unix-ag.uni-kl.de>
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#     *  Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#     *  Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#     *  Neither the name of Thomas Fischer nor the names of other contributors
#        may be used to endorse or promote products derived from this software
#        without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
#  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

umask 0077 # configuration files are sensitive ...

TEMPDIR=$(mktemp --tmpdir -d fix-configuration-files-XXXXXXXXX.d)
CFGNEWFILESLIST=$(mktemp --tmpdir fix-configuration-files-new-files-XXXXXXXXXXX)
CFGSAVEFILESLIST=$(mktemp --tmpdir fix-configuration-files-saved-files-XXXXXXXXXXX)
CFGORIGFILESLIST=$(mktemp --tmpdir fix-configuration-files-orig-files-XXXXXXXXXXX)
function cleanup_on_exit {
	rm -rf ${TEMPDIR} ${CFGNEWFILESLIST} ${CFGSAVEFILESLIST} ${CFGORIGFILESLIST}
}
trap cleanup_on_exit EXIT

function error() {
	local msg="$1"
	echo -ne "\e[31m" >&2
	echo -n "${msg}" >&2
	echo -e "\e[0m" >&2
	exit 1
}

function warn() {
	local msg="$1"
	echo -ne "\e[1;33m" >&2
	echo -n "${msg}" >&2
	echo -e "\e[0m" >&2
}
function info() {
	local msg="$1"
# 	echo -ne "\e[1;97m"
	echo -n "${msg}"
	echo -e "\e[0m"
}

IS_ARCH=0
IS_RPM=0
IS_GENTOO=0
IS_DEBIAN=0
test "$0" != "${0/pacnew/}" && IS_ARCH=1
test "$0" != "${0/rpmnew/}" && IS_RPM=1
test "$0" != "${0/gentoo/}" && IS_GENTOO=1
test "$0" != "${0/debian/}" && IS_DEBIAN=1

test $(( ${IS_ARCH} + ${IS_RPM} + ${IS_GENTOO} + ${IS_DEBIAN} )) -eq 1 || error "Could not detect platform (ArchLinux, RPM-based, Gentoo, or Debian)"

function mergefiles() {
	local primaryfile="$1"
	local secondaryfile="$2"
	local mergedfile="$3"
	local referencefile="${4:-${primaryfile}}"

	test "${secondaryfile}" = "${primaryfile}" && return
	sudo test -f "${secondaryfile}" || { info "File '${secondaryfile}' no longer exists, skipping" ; return ; }
	sudo test -f "${primaryfile}" || {
		warn "File '${primaryfile}' does not exist"
		sudo test -s "${secondaryfile}" || error "File '${secondaryfile}' does not exist or is empty"
		sudo rm -f "${secondaryfile}" || error "File '${secondaryfile}' could not be removed"
		return # no more processing required
	}
	sudo diff -q "${primaryfile}" "${secondaryfile}" && {
		info "Files '${primaryfile}' and '${secondaryfile}' are identical"
		sudo test -s "${secondaryfile}" || error "File '${secondaryfile}' does not exist or is empty"
		sudo rm -f "${secondaryfile}" || error "File '${secondaryfile}' could not be removed"
		return # no more processing required
	}

	# Just to be sure that directory is empty after previous iterations
	rm -rf ${TEMPDIR} ; mkdir -p ${TEMPDIR} || error "Could not create temporary directory '${TEMPDIR}'"
	chmod 0700 ${TEMPDIR} || error "Could not set permissions for temporary directory '${TEMPDIR}'"
	sudo cp "${primaryfile}" "${secondaryfile}" ${TEMPDIR}/ || error "Could not copy files '${primaryfile}' and '${secondaryfile}' to '${TEMPDIR}'"
	local USERID=$(id -u)
	local GROUPID=$(id -g)
	sudo chown -R ${USERID}:${GROUPID} ${TEMPDIR} || error "Could not set permissions for files in temporary directory"
	find ${TEMPDIR} -type f -exec sudo chmod 0600 '{}' ';' || error "Could not set permissions for files in temporary directory"

	info "Merging files"
	ls -1a ${TEMPDIR}/* ${TEMPDIR}/._cfg* 2>/dev/null | while read line ; do echo "  ${line}" ; done
	echo "into ${mergedfile}"
	find ${TEMPDIR} -type f -print0 | xargs -0 kdiff3 -m -o "${mergedfile}"
	local kdiff3exitcode=$?
	ls -l "${mergedfile}"
	if [[ $? -eq 0 && -s "${mergedfile}" ]] ; then
		sudo chown --reference "${referencefile}" "${mergedfile}" || error "Could not set permissions for merged file"
		sudo chmod --reference "${referencefile}" "${mergedfile}" || error "Could not set permissions for merged file"
		sudo cp "${mergedfile}" "${primaryfile}" || error "Could not copy merged file to destination '${primaryfile}'"
		sudo test -s "${secondaryfile}" || error "File '${secondaryfile}' does not exist or is empty"
		sudo rm -f "${secondaryfile}" || error "File '${secondaryfile}' could not be removed"
		return # no more processing required
	else
		error "Running 'kdiff3' failed or merged file created"
	fi
}

## === .pacnew/.rpmnew files ===

test $IS_ARCH   -gt 0 && locate -e --regex "\.pacnew$" >${CFGNEWFILESLIST} 2>/dev/null
test $IS_RPM    -gt 0 && locate -e --regex "\.rpmnew$" >${CFGNEWFILESLIST} 2>/dev/null
test $IS_GENTOO -gt 0 && locate -e --regex "^\._cfg[0-9]+_" >${CFGNEWFILESLIST} 2>/dev/null
test $IS_DEBIAN -gt 0 && locate -e --regex "\.dpkg-dist$" >${CFGNEWFILESLIST} 2>/dev/null
test $IS_ARCH   -gt 0 && grep -ha --color=NEVER -E -o '/[^ ]+\.pacnew\b' /var/log/pacman.log >>${CFGNEWFILESLIST} 2>/dev/null
test $IS_RPM    -gt 0 && grep -ha --color=NEVER -E -o '/[^ ]+\.rpmnew\b' /var/log/upgrade.log /var/log/zypper.log /var/log/zypp/history >>${CFGNEWFILESLIST} 2>/dev/null
test $IS_ARCH   -gt 0 && sudo find /etc -maxdepth 4 -type f -name '*.pacnew' >>${CFGNEWFILESLIST} 2>/dev/null
test $IS_RPM    -gt 0 && sudo find /etc -maxdepth 4 -type f -name '*.rpmnew' >>${CFGNEWFILESLIST} 2>/dev/null
test $IS_GENTOO -gt 0 && sudo find /etc -maxdepth 4 -type f -name '._cfg0*_*' >>${CFGNEWFILESLIST} 2>/dev/null
test $IS_DEBIAN -gt 0 && sudo find /etc -maxdepth 4 -type f -name '*.dpkg-dist' >>${CFGNEWFILESLIST} 2>/dev/null

sort -u <${CFGNEWFILESLIST} | while read newconfigfile ; do
	test $IS_ARCH   -gt 0 && currconfigfile="${newconfigfile/.pacnew/}"
	test $IS_RPM    -gt 0 && currconfigfile="${newconfigfile/.rpmnew/}"
	test $IS_GENTOO -gt 0 && currconfigfile=$(sed -e 's/\._cfg[0-9]*_//' <<<${newconfigfile})
	test $IS_DEBIAN -gt 0 && currconfigfile="${newconfigfile/.dpkg-dist/}"

	test $IS_ARCH   -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${newconfigfile/.pacnew/.merged})
	test $IS_RPM    -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${newconfigfile/.rpmnew/.merged})
	test $IS_GENTOO -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${currconfigfile}).merged
	test $IS_DEBIAN -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${newconfigfile/.dpkg-dist/.merged})

	mergefiles "${currconfigfile}" "${newconfigfile}" "${mergedconfigfile}" "${newconfigfile}"
done


# ## === .pacsave/.rpmsave files ===

test $IS_ARCH   -gt 0 && locate -e --regex "\.pacsave$" >${CFGSAVEFILESLIST} 2>/dev/null
test $IS_RPM    -gt 0 && locate -e --regex "\.rpmsave$" >${CFGSAVEFILESLIST} 2>/dev/null
test $IS_DEBIAN -gt 0 && locate -e --regex "\.dpkg-old$" >${CFGSAVEFILESLIST} 2>/dev/null
test $IS_ARCH   -gt 0 && grep -ha --color=NEVER -E -o '/[^ ]+\.pacsave\b' /var/log/pacman.log >>${CFGSAVEFILESLIST} 2>/dev/null
test $IS_RPM    -gt 0 && grep -ha --color=NEVER -E -o '/[^ ]+\.rpmsave\b' /var/log/upgrade.log /var/log/zypper.log /var/log/zypp/history >>${CFGSAVEFILESLIST} 2>/dev/null
test $IS_ARCH   -gt 0 && sudo find /etc -maxdepth 4 -type f -name '*.pacsave' >>${CFGSAVEFILESLIST} 2>/dev/null
test $IS_RPM    -gt 0 && sudo find /etc -maxdepth 4 -type f -name '*.rpmsave' >>${CFGSAVEFILESLIST} 2>/dev/null
test $IS_DEBIAN -gt 0 && sudo find /etc -maxdepth 4 -type f -name '*.dpkg-old' >>${CFGSAVEFILESLIST} 2>/dev/null

sort -u <${CFGSAVEFILESLIST} | while read oldconfigfile ; do
	test $IS_ARCH   -gt 0 && currconfigfile="${oldconfigfile/.pacsave/}"
	test $IS_RPM    -gt 0 && currconfigfile="${oldconfigfile/.rpmsave/}"
	test $IS_DEBIAN -gt 0 && currconfigfile="${oldconfigfile/.dpkg-old/}"

	test $IS_ARCH   -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${oldconfigfile/.pacsave/.merged})
	test $IS_RPM    -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${oldconfigfile/.rpmsave/.merged})
	test $IS_DEBIAN -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${oldconfigfile/.dpkg-old/.merged})

	mergefiles "${currconfigfile}" "${oldconfigfile}" "${mergedconfigfile}" "${currconfigfile}"
done


## === .pacorig/.rpmorig files ===

test $IS_ARCH -gt 0 && locate -e --regex "\.pacorig$" >${CFGORIGFILESLIST} 2>/dev/null
test $IS_RPM  -gt 0 && locate -e --regex "\.rpmorig$" >${CFGORIGFILESLIST} 2>/dev/null
test $IS_ARCH -gt 0 && grep -ha --color=NEVER -E -o '/[^ ]+\.pacorig\b' /var/log/pacman.log >>${CFGORIGFILESLIST} 2>/dev/null
test $IS_RPM  -gt 0 && grep -ha --color=NEVER -E -o '/[^ ]+\.rpmorig\b' /var/log/pacman.log >>${CFGORIGFILESLIST} 2>/dev/null
test $IS_ARCH -gt 0 && find /etc -maxdepth 4 -type f -name '*.pacorig' >>${CFGORIGFILESLIST} 2>/dev/null
test $IS_RPM  -gt 0 && find /etc -maxdepth 4 -type f -name '*.rpmorig' >>${CFGORIGFILESLIST} 2>/dev/null

sort -u <${CFGORIGFILESLIST} | while read origconfigfile ; do
	test $IS_ARCH -gt 0 && currconfigfile="${origconfigfile/.pacorig/}"
	test $IS_RPM  -gt 0 && currconfigfile="${origconfigfile/.rpmorig/}"

	test $IS_ARCH   -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${origconfigfile/.pacorig/.merged})
	test $IS_RPM    -gt 0 && mergedconfigfile=${TEMPDIR}/$(basename ${origconfigfile/.rpmorig/.merged})

	mergefiles "${currconfigfile}" "${origconfigfile}" "${mergedconfigfile}" "${currconfigfile}"
done

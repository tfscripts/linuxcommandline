/* Copyright (c) 2011-2019, Thomas Fischer <fischer@unix-ag.uni-kl.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Thomas Fischer nor the names of other contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _POSIX_C_SOURCE 200809L

#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>

#define MAX_NUM_COMMAND_LINES   131072
#define MAX_COMMAND_LENGTH       16384
#define MAX_ARGUMENT_LENGTH      16384
#define MAX_NUM_WORKER_THREADS      32

#define DATETIME_FORMAT_STR    "%F@%T"

#define strncpyzero(dest,src,len) strncpy((dest),(src),(len));(dest)[(len)-1]='\0';

/// interpreter to run command lines with
char interpreter[MAX_COMMAND_LENGTH];

/// array to store all command lines
char *command_lines[MAX_NUM_COMMAND_LINES];

/// number of used command lines = number of commands
int num_commands;

/// next command to be processed
/// variable is shared among threads, has to be protected by a mutex
int cur_command_line;

/// stop thread execution once this command number is reached
int num_command_limit;

/// array of pre-allocated threads
pthread_t workerthread[MAX_NUM_WORKER_THREADS];

/// single mutex to protect variables shared among worker threads
pthread_mutex_t workermutex;

/// single mutex to protect output
pthread_mutex_t outputmutex;

/// global variable to store verbosity level
int be_verbose;

/// global variable to store silent mode flag
int be_silent;

/// global variable to store color or no color flag
int color_output;

/// global variable with boolean flag whether to exit if a child exits with non-zero exit code
int exit_on_failed_child;
int global_exit_code;

/// base path for text files containing stdout and stderr redirections
char output_path[MAX_ARGUMENT_LENGTH];

/// how many threads have been started?
/// variable is shared among threads, has to be protected by a mutex
int thread_id;

/// number of instanciated threads
int num_threads;

/// number of running threads, increased and decreased in each thread
/// variable is shared among threads, has to be protected by a mutex
int num_running_threads;

/// Static string buffer to hold the current time
char time_str[MAX_ARGUMENT_LENGTH];

void print_english_ordinal(char *buffer, size_t buffer_size, int number)
{
    if (number % 10 == 1 && number != 11)
        snprintf(buffer, buffer_size, "%dst", number);
    else if (number % 10 == 2 && number != 12)
        snprintf(buffer, buffer_size, "%dnd", number);
    else if (number % 10 == 3 && number != 13)
        snprintf(buffer, buffer_size, "%drd", number);
    else
        snprintf(buffer, buffer_size, "%dth", number);
}

void update_time_str()
{
    time_t curtime = time(NULL);
    struct tm *loctime = localtime(&curtime);

    strftime(time_str, MAX_ARGUMENT_LENGTH - 1, DATETIME_FORMAT_STR, loctime);
}

time_t starttime;
char etatime_str[MAX_ARGUMENT_LENGTH];

void update_etatime_str()
{
    const time_t curtime = time(NULL);
    const time_t deltatime = curtime - starttime;

    if (deltatime < 60 || cur_command_line < num_threads || cur_command_line < (num_commands >> 3)) {
        const int n = snprintf(etatime_str, MAX_ARGUMENT_LENGTH - 1, "Uncertain, need more data");
        if (n < 0) {
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            fprintf(stderr, "%sFailed to assemble ETA string%s\n", enter_color, exit_color);
            etatime_str[0] = '\0';
            return;
        }
        return;
    }

    const time_t etatime = starttime + deltatime * num_commands / cur_command_line;
    struct tm *loctime = localtime(&etatime);
    strftime(etatime_str, MAX_ARGUMENT_LENGTH - 1, DATETIME_FORMAT_STR, loctime);
}

int run_command(int id, char *full_command, char *output_prefix)
{
    pid_t pid;
    int exit_code = 0;

    char script_name[MAX_ARGUMENT_LENGTH];
    char *tempdir_environment = getenv("TMPDIR");
    if (tempdir_environment == NULL)
        tempdir_environment = getenv("TEMPDIR");
    if (tempdir_environment == NULL || tempdir_environment[0] == '\0') {
        const int n = snprintf(script_name, MAX_ARGUMENT_LENGTH - 1, "/tmp/.queue-%d-XXXXXX", getpid());
        if (n < 0) {
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            fprintf(stderr, "%sFailed to assemble filename for temporary script%s\n", enter_color, exit_color);
            return -1;
        }
    } else {
        const int n = snprintf(script_name, MAX_ARGUMENT_LENGTH - 1, "%s/.queue-%d-XXXXXX", tempdir_environment, getpid());
        if (n < 0) {
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            fprintf(stderr, "%sFailed to assemble filename for temporary script%s\n", enter_color, exit_color);
            return -1;
        }
    }

    int fd = mkstemp(script_name);
    if (fd < 0) {
        const int e = errno;
        pthread_mutex_lock(&outputmutex);
        update_time_str();
        const char *enter_color = color_output ? "\033[1;31m" : "";
        const char *exit_color = color_output ? "\033[0m" : "";
        fprintf(stderr, "%s [%s]: %sFailed to create temporary file%s: %s (pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, script_name, getpid(), e, strerror(e));
        pthread_mutex_unlock(&outputmutex);
        exit(1);
    }

    if ((pid = fork()) == -1) {
        const int e = errno;
        pthread_mutex_lock(&outputmutex);
        update_time_str();
        const char *enter_color = color_output ? "\033[1;31m" : "";
        const char *exit_color = color_output ? "\033[0m" : "";
        fprintf(stderr, "%s [%s]: %sFailed to fork%s (pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, getpid(), e, strerror(e));
        pthread_mutex_unlock(&outputmutex);
        exit(1);
    } else if (pid == 0) {
        /// Inside child

        char internal_stdout_filename[MAX_ARGUMENT_LENGTH];
        char internal_stderr_filename[MAX_ARGUMENT_LENGTH];
        int output_file_mode = be_silent > 0 ? O_WRONLY : O_CREAT | O_WRONLY;

        if (be_silent) {
            snprintf(internal_stdout_filename, MAX_ARGUMENT_LENGTH - 1, "/dev/null");
            snprintf(internal_stderr_filename, MAX_ARGUMENT_LENGTH - 1, "/dev/null");
        } else {
            const int n = snprintf(internal_stdout_filename, MAX_ARGUMENT_LENGTH - 1, "%s/prog%05d-stdout.txt", output_path, id);
            if (n < 0) {
                const char *enter_color = color_output ? "\033[1;31m" : "";
                const char *exit_color = color_output ? "\033[0m" : "";
                fprintf(stderr, "%sFailed to assemble filename standard output of job %d%s\n", enter_color, id, exit_color);
                exit(1);
            } else {
                const int n = snprintf(internal_stderr_filename, MAX_ARGUMENT_LENGTH - 1, "%s/prog%05d-stderr.txt", output_path, id);
                if (n < 0) {
                    const char *enter_color = color_output ? "\033[1;31m" : "";
                    const char *exit_color = color_output ? "\033[0m" : "";
                    fprintf(stderr, "%sFailed to assemble filename error output of job %d%s\n", enter_color, id, exit_color);
                    exit(1);
                }
            }

            unlink(internal_stdout_filename);
            unlink(internal_stderr_filename);
        }

        int prog_stdout = open(internal_stdout_filename, output_file_mode, S_IRUSR | S_IWUSR);
        if (prog_stdout >= 0) {
            close(1);
            dup2(prog_stdout, 1);
        }
        int prog_stderr = open(internal_stderr_filename, output_file_mode, S_IRUSR | S_IWUSR);
        if (prog_stderr >= 0) {
            close(2);
            dup2(prog_stderr, 2);
        }

        FILE *script = fdopen(fd, "w");
        if (script != NULL) {
            if (fputs(full_command, script) > 0 && fputs("\n", script) > 0) {
                if (fclose(script) == 0) {
                    char *arguments[3];
                    arguments[0] = interpreter;
                    arguments[1] = script_name;
                    arguments[2] = NULL;
                    execvp(arguments[0], arguments);
                    const int e = errno;
                    pthread_mutex_lock(&outputmutex);
                    update_time_str();
                    const char *enter_color = color_output ? "\033[1;31m" : "";
                    const char *exit_color = color_output ? "\033[0m" : "";
                    fprintf(stderr, "%s [%s]: %sFailed to run command%s: %s (pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, full_command, getpid(), e, strerror(e));
                    pthread_mutex_unlock(&outputmutex);
                    exit(1);
                } else {
                    const int e = errno;
                    pthread_mutex_lock(&outputmutex);
                    update_time_str();
                    const char *enter_color = color_output ? "\033[1;31m" : "";
                    const char *exit_color = color_output ? "\033[0m" : "";
                    fprintf(stderr, "%s [%s]: %sCould not close temporary file%s: %s (pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, script_name, getpid(), e, strerror(e));
                    pthread_mutex_unlock(&outputmutex);
                    exit(1);
                }
            } else {
                const int e = errno;
                fclose(script);
                pthread_mutex_lock(&outputmutex);
                update_time_str();
                const char *enter_color = color_output ? "\033[1;31m" : "";
                const char *exit_color = color_output ? "\033[0m" : "";
                fprintf(stderr, "%s [%s]: %sCould not write script to temporary file%s: %s (pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, script_name, getpid(), e, strerror(e));
                pthread_mutex_unlock(&outputmutex);
                exit(1);
            }
        } else {
            const int e = errno;
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            fprintf(stderr, "%s [%s]: %sUnable to open temporary file%s: %s (pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, script_name, getpid(), e, strerror(e));
            pthread_mutex_unlock(&outputmutex);
            exit(1);
        }
    } else {
        /// Inside parent, child's PID is in variable 'pid'

        char ordinal_buffer[MAX_ARGUMENT_LENGTH];
        print_english_ordinal(ordinal_buffer, MAX_ARGUMENT_LENGTH - 1, id);

        if (be_verbose > 1) {
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            printf("%s [%s]: waiting for %s fork'ed child process %i\n", output_prefix, time_str, ordinal_buffer, pid);
            pthread_mutex_unlock(&outputmutex);
        }
        int p = -1;
        int status = 0;
        if ((p = waitpid(pid, &status, 0)) != pid) {
            const int e = errno;
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            fprintf(stderr, "%s [%s]: %sSomething went wrong while waiting for %s child process %d%s (returned pid=%d, errno=%d)\n", output_prefix, time_str, enter_color, ordinal_buffer, pid, exit_color, p, e);
            pthread_mutex_unlock(&outputmutex);
            exit_code = WEXITSTATUS(status);
            if (exit_code == 0) exit_code = 1; ///< in case WEXITSTATUS is 0 (no error), set it to 1 (some error)
        } else if (WIFEXITED(status)) {
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            const int exitstatus = WEXITSTATUS(status);
            const char *enter_color = color_output ? (exitstatus == 0 ? "\033[1;32m" : "\033[1;31m") : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            printf("%s [%s]: %s child process %i %sexited with status %d%s\n", output_prefix, time_str, ordinal_buffer, pid, enter_color, exitstatus, exit_color);
            pthread_mutex_unlock(&outputmutex);
            exit_code = WEXITSTATUS(status);
        } else if (WIFSIGNALED(status)) {
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            printf("%s [%s]: %s child process %i %sexited due to signal %d%s\n", output_prefix, time_str, ordinal_buffer, pid, enter_color, WTERMSIG(status), exit_color);
            pthread_mutex_unlock(&outputmutex);
            exit_code = WEXITSTATUS(status);
            if (exit_code == 0) exit_code = 1; ///< in case WEXITSTATUS is 0 (no error), set it to 1 (some error)
        } else {
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            printf("%s [%s]: %s child process %i %sexited due to an unknown reason%s\n", output_prefix, time_str, ordinal_buffer, pid, enter_color, exit_color);
            pthread_mutex_unlock(&outputmutex);
            exit_code = WEXITSTATUS(status);
            if (exit_code == 0) exit_code = 1; ///< in case WEXITSTATUS is 0 (no error), set it to 1 (some error)
        }
        if (close(fd) == 0) {
            if (unlink(script_name) == 0) {
                /// successfully closed and removed temporary file
            } else {
                const int e = errno;
                pthread_mutex_lock(&outputmutex);
                update_time_str();
                const char *enter_color = color_output ? "\033[1;31m" : "";
                const char *exit_color = color_output ? "\033[0m" : "";
                fprintf(stderr, "%s [%s]: %sremoving temporary file failed%s: %s (id=%d, pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, script_name, id, getpid(), e, strerror(e));
                pthread_mutex_unlock(&outputmutex);
            }
        } else {
            const int e = errno;
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            const char *enter_color = color_output ? "\033[1;31m" : "";
            const char *exit_color = color_output ? "\033[0m" : "";
            fprintf(stderr, "%s [%s]: %sclosing temporary file failed%s: %s (id=%d, pid=%d, errno=%d: %s)\n", output_prefix, time_str, enter_color, exit_color, script_name, id, getpid(), e, strerror(e));
            pthread_mutex_unlock(&outputmutex);
        }
    }

    if (be_verbose) {
        /// In verbose mode, write brief log file with key arguments
        char log_filename[MAX_ARGUMENT_LENGTH];
        const int n = snprintf(log_filename, MAX_ARGUMENT_LENGTH - 1, "%s/prog%05d-log.txt", output_path, id);
        if (n > 0) {
            FILE *logfile = fopen(log_filename, "w");
            if (logfile != NULL) {
                fprintf(logfile, "id = %d\n", id);
                fprintf(logfile, "pid = %d\n", pid);
                fprintf(logfile, "ppid = %d\n", getpid());
                fprintf(logfile, "exit_code = %d\n", exit_code);
                fprintf(logfile, "full_command = %s\n", full_command);
                fclose(logfile);
            }
        }
    }

    return exit_code;
}

void *run_command_function(void *x)
{
    int id = -1;

    pthread_mutex_lock(&workermutex);
    id = thread_id++;
    ++num_running_threads;
    pthread_mutex_unlock(&workermutex);

    char output_prefix[MAX_ARGUMENT_LENGTH];
    snprintf(output_prefix, MAX_ARGUMENT_LENGTH - 1, "Thread %i/%i", id + 1, num_threads);

    char *full_command;

    if (be_verbose > 2) {
        pthread_mutex_lock(&outputmutex);
        update_time_str();
        printf("%s [%s]: starting\n", output_prefix, time_str);
        pthread_mutex_unlock(&outputmutex);
    }
    int old_cur = -1;
    pthread_mutex_lock(&workermutex);
    old_cur = cur_command_line >= num_command_limit ? num_command_limit : cur_command_line++;
    pthread_mutex_unlock(&workermutex);
    while (global_exit_code == 0 && old_cur < num_command_limit && (full_command = command_lines[old_cur]) != NULL) {
        if (be_verbose > 0) {
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            printf("%s [%s]: running %4i/%4i", output_prefix, time_str, old_cur + 1, num_commands);
            if (be_verbose > 1)
                printf(": %s", full_command);
            printf("\n");
            pthread_mutex_unlock(&outputmutex);
        }

        const int exit_code = run_command(old_cur, full_command, output_prefix);
        if (exit_on_failed_child && exit_code != 0) global_exit_code = exit_code; ///< update global variable that some child failed

        if (be_verbose > 0) {
            pthread_mutex_lock(&outputmutex);
            update_time_str();
            update_etatime_str();
            printf("%s [%s, ETA: %s]: finished job", output_prefix, time_str, etatime_str);
            if (be_verbose > 1)
                printf(": %s", full_command);
            printf("\n");
            pthread_mutex_unlock(&outputmutex);
        }

        pthread_mutex_lock(&workermutex);
        old_cur = cur_command_line >= num_command_limit ? num_command_limit : cur_command_line++;
        pthread_mutex_unlock(&workermutex);
    }
    if (be_verbose > 0) {
        pthread_mutex_lock(&outputmutex);
        update_time_str();
        printf("%s [%s]: exiting\n", output_prefix, time_str);
        pthread_mutex_unlock(&outputmutex);
    }

    pthread_mutex_lock(&workermutex);
    --num_running_threads;
    if (be_verbose > 0) {
        pthread_mutex_lock(&outputmutex);
        update_time_str();
        update_etatime_str();
        printf("%s [%s, ETA: %s]: num threads still running: %i\n", output_prefix, time_str, etatime_str, num_running_threads);
        pthread_mutex_unlock(&outputmutex);
    }
    pthread_mutex_unlock(&workermutex);

    pthread_exit(NULL);
}

int read_command_lines_file(char *filename, int result)
{
    char buffer[MAX_COMMAND_LENGTH];
    FILE *f = (filename[0] == '-' && filename[1] == '\0') ? stdin : fopen(filename, "r");
    if (f != NULL) {
        while (fgets(buffer, MAX_COMMAND_LENGTH - 4, f) != NULL && result < MAX_NUM_COMMAND_LINES - 1) {
            size_t len = strlen(buffer);
            if (len > 1 && buffer[0] != '#') {
                buffer[len - 1] = '\0';
                command_lines[result++] = strndup(buffer, MAX_COMMAND_LENGTH);
            }
        }
        if (f != stdin) fclose(f);
    } else {
        const char *enter_color = color_output ? "\033[1;31m" : "";
        const char *exit_color = color_output ? "\033[0m" : "";
        fprintf(stderr, "%sCould not open file%s: %s\n", enter_color, exit_color, filename);
        return result;
    }

    return result;
}

int main(int argc, char *argv[])
{
    strncpyzero(interpreter, getenv("SHELL"), MAX_COMMAND_LENGTH - 1);
    output_path[0] = '.';
    output_path[1] = '\0';
    be_verbose = 0;
    be_silent = 0;
    color_output = isatty(fileno(stdout)); ///< enable color on interactive terminals, not if output is redirected (e.g. pipe)
    exit_on_failed_child = 0;
    global_exit_code = 0;
    const int num_physical_cores = (int)(sysconf(_SC_NPROCESSORS_ONLN) & 0x0fffff);
    num_threads = num_physical_cores;
    if (num_threads < 1) num_threads = 1;
    if (num_threads > MAX_NUM_WORKER_THREADS) num_threads = MAX_NUM_WORKER_THREADS;
    num_commands = 0;
    int i;
    for (i = MAX_NUM_COMMAND_LINES - 1; i >= 0; --i)
        command_lines[i] = NULL;

    for (i = 1; i < argc; ++i) {
        if (argv[i][0] == '-') {
            if (argv[i][1] == 'h' || (argv[i][1] == '-' && argv[i][2] == 'h')) {
                printf("queue [-h] [-V] [-s|-p PATH] [-x] [-v] [-i CMD] [-j n] file1 [file2 [...]]\n");
                printf("  -h       Show this help\n");
                printf("  -V       Verbose output, multipe -V increase verbosity\n");
                printf("  -c       Enable color output even if not in interactive terminal\n");
                printf("  -C       Disable color output even if in interactive terminal\n");
                printf("  -s       Silent mode, omit writing stdout/stderr files\n");
                printf("  -p PATH  Write stdout/stderr files there instead of CWD\n");
                printf("  -x       If a queued process exits with non-zero exit code, do not schedule new jobs;\n");
                printf("           queue will let running jobs finish and then exit with same exit code\n");
                printf("  -v       Show copyright information\n");
                printf("  -i CMD   Interpreter used to run commands, default is '%s' ($SHELL)\n", interpreter);
                printf("  -j n     Specify number of threads, default is %i\n", num_threads);
                printf("  fileX    Text file with one command plus arguments per line\n");
                printf("           A line only containing the word 'WAIT' will force all threads to join before\n");
                printf("           resuming to execute any later commands\n");
                return 1;
            } else if (argv[i][1] == 'j') {
                if (argv[i][2] != '\0') {
                    num_threads = atoi(argv[i] + 2);
                } else if (i < argc + 1) {
                    ++i;
                    num_threads = atoi(argv[i]);
                }
            } else if (argv[i][1] == 'V')
                ++be_verbose;
            else if (argv[i][1] == 's')
                ++be_silent;
            else if (argv[i][1] == 'C')
                color_output = 0;
            else if (argv[i][1] == 'c')
                color_output = 1;
            else if (argv[i][1] == 'x')
                ++exit_on_failed_child;
            else if (argv[i][1] == 'p') {
                if (argv[i][2] != '\0') {
                    strncpyzero(output_path, argv[i] + 2, MAX_ARGUMENT_LENGTH);
                } else if (i < argc + 1) {
                    ++i;
                    strncpyzero(output_path, argv[i], MAX_ARGUMENT_LENGTH);
                }
                size_t l = strlen(output_path);
                if (output_path[l - 1] == '/') output_path[l - 1] = '\0';
            } else if (argv[i][1] == 'v' || (argv[i][1] == '-' && argv[i][2] == 'v')) {
                printf("Copyright (c) 2011-2019, Thomas Fischer <fischer@unix-ag.uni-kl.de>\n");
                printf("All rights reserved.\n");
                printf("\nReleased under the 3-clause BSD license.\n");
                printf("https://www.gitlab.com/tfscripts/linuxcommandline\n");
                return 0;
            } else if (argv[i][1] == 'i' && i < argc + 1) {
                if (argv[i + 1][0] == '/') {
                    strncpyzero(interpreter, argv[i + 1], MAX_COMMAND_LENGTH);
                } else {
                    fprintf(stderr, "Interpreter command has to start with '/' (i.e. absolute path),\n");
                    fprintf(stderr, "therefore using '%s' instead of '%s'.\n", interpreter, argv[i + 1]);
                }
                ++i;
            } else {
                const char *enter_color = color_output ? "\033[1;31m" : "";
                const char *exit_color = color_output ? "\033[0m" : "";
                fprintf(stderr, "%sUnknown option%s: %s\n", enter_color, exit_color, argv[i]);
            }
        } else
            num_commands = read_command_lines_file(argv[i], num_commands);
    }

    if (num_threads < 1) num_threads = 1;
    if (num_threads > MAX_NUM_WORKER_THREADS) num_threads = MAX_NUM_WORKER_THREADS;

    if (be_verbose > 0) {
        printf("num_commands=%i\n", num_commands);
        printf("num_threads=%i\n", num_threads);
        printf("interpreter=%s\n", interpreter);
        printf("output_path=%s\n", output_path);
        printf("exit on failed child=%s\n", exit_on_failed_child ? "yes" : "no");
    }

    struct stat statbuf;
    if (stat(output_path, &statbuf) != 0) {
        const char *enter_color = color_output ? "\033[1;31m" : "";
        const char *exit_color = color_output ? "\033[0m" : "";
        fprintf(stderr, "%sCannot probe path for stdout/stderr%s: %s", enter_color, exit_color, output_path);
        return 1;
    } else if (!S_ISDIR(statbuf.st_mode)) {
        const char *enter_color = color_output ? "\033[1;31m" : "";
        const char *exit_color = color_output ? "\033[0m" : "";
        fprintf(stderr, "%sPath for stdout/stderr is not a directory%s: %s", enter_color, exit_color, output_path);
        return 1;
    } else if (access(output_path, R_OK | W_OK | X_OK) != 0) {
        const char *enter_color = color_output ? "\033[1;31m" : "";
        const char *exit_color = color_output ? "\033[0m" : "";
        fprintf(stderr, "%sInsufficient permissions for path for stdout/stderr%s: %s", enter_color, exit_color, output_path);
        return 1;
    }

    time(&starttime);

    /// If there are more than one command to process ...
    if (num_commands > 0) {
        cur_command_line = 0; ///< start with first command
        /// While not done with all queued commands ...
        while (global_exit_code == 0 && cur_command_line < num_commands) {
            /// Peak ahead for next line that starts with 'WAIT' (or end of queue of commands)
            for (num_command_limit = cur_command_line; num_command_limit < num_commands; ++num_command_limit)
                if (command_lines[num_command_limit][0] == 'W' && command_lines[num_command_limit][1] == 'A' && command_lines[num_command_limit][2] == 'I' && command_lines[num_command_limit][3] == 'T' && command_lines[num_command_limit][4] <= 0x20)
                    break;
            /// Now, 'num_command_limit' holds the next line with 'WAIT' or the end position of the queue of commands

            /// Check if there are commands to be processed (e.g. skip two subsequent 'WAIT' lines)
            if (cur_command_line < num_command_limit) {
                /// Launch all threads
                thread_id = 0;
                for (i = 0; i < num_threads; ++i)
                    pthread_create(&workerthread[i], NULL, run_command_function, NULL);
                /// Wait for all threads to join
                for (i = 0; i < num_threads; ++i)
                    pthread_join(workerthread[i], NULL);
            }

            /// Next command for next while iteration, after 'WAIT' in line 'num_command_limit'
            cur_command_line = num_command_limit + 1;

            /// If more commands to follow, this means there was a WAIT statement
            /// So, if in verbose mode, notify user about this
            if (cur_command_line < num_commands && be_verbose > 0)
                printf("Encountered WAIT statement, synchronizing threads\n");
        }
    }

    if (be_verbose > 0 && global_exit_code != 0) {
        const char *enter_color = color_output ? "\033[1;31m" : "";
        const char *exit_color = color_output ? "\033[0m" : "";
        fprintf(stderr, "%sExiting as some queued process exited with exit code %d%s\n", enter_color, global_exit_code, exit_color);
    }

    for (i = MAX_NUM_COMMAND_LINES - 1; i >= 0; --i)
        if (command_lines[i] != NULL) free(command_lines[i]);

    return global_exit_code; ///< global exit code will only be not zero if 'exit_on_failed_child' was true and some child actually failed
}

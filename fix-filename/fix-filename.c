#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

static const size_t maxstrlen = 8192;

int main(int argc, char *argv[])
{
    int error = 0;
    for (int i = 1; error == 0 && i < argc; ++i) {
        char *origname = argv[i];
        char *newname = strndup(origname, maxstrlen);
        size_t len = strlen(newname);
        for (int p = 0; p < len;) {
            if (p < len - 1) {
                if (newname[p] == '\xc2') {
                    if (newname[p + 1] == '\x94') {
                        newname[p] = '\xc3';
                        newname[p + 1] = '\xb6';
                        p += 2;
                    } else if (newname[p + 1] == '\x84') {
                        newname[p] = '\xc3';
                        newname[p + 1] = '\xa4';
                        p += 2;
                    } else
                        ++p;
                } else if (newname[p] == '+') {
                    if (newname[p + 1] == '\xd1') {
                        newname[p] = '\xc3';
                        newname[p + 1] = '\xa5';
                        p += 2;
                    } else if (newname[p] == '+' && newname[p + 1] == '\xf1') {
                        newname[p] = '\xc3';
                        newname[p + 1] = '\xa4';
                        p += 2;
                    } else if (newname[p] == '+' && newname[p + 1] == '\xc2') {
                        newname[p] = '\xc3';
                        newname[p + 1] = '\xb6';
                        p += 2;
                    } else
                        ++p;
                } else
                    ++p;
            } else
                ++p;
        }
        if (strncmp(newname, origname, maxstrlen) != 0) {
            error = rename(origname, newname);
        }
        free(newname);
    }
    return 0;
}

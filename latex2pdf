#!/usr/bin/env bash

# Temporary directory used for compilation
TEMPDIR=$(mktemp -t -d tmp.latex2pdf.XXXXXXXXXXXXX.d)

# Clean-up function removing temporary directory
function cleanup {
	rm -rf ${TEMPDIR}
}
# Call clean-up function on script exit or if terminated/killed
trap cleanup EXIT SIGTERM SIGKILL

# Check argument
if [[ -z "$1" ]] ; then
	echo "No argment passed" >&2
	exit 1
elif [[ "$1" != *.tex ]] ; then
	echo "Filename does not end with '.tex'" >&2
	exit 1
elif [[ ! -f "$1" ]] ; then
	echo "File '$1' does not exist" >&2
	exit 1
fi

# Test if to call lualatex or pdflatex based on if package 'fontspec' is used
LUALATEX=0
head -n 100 "$1" | grep -q -E '^\\usepackage.*\{fontspec\}' && LUALATEX=1

if [[ $LUALATEX -gt 0 ]] ; then
	# Run lualatex to compile .tex document
	lualatex -halt-on-error -output-directory ${TEMPDIR} "$1" || exit 1
	lualatex -halt-on-error -output-directory ${TEMPDIR} "$1" || exit 1
else
	# Run pdflatex to compile .tex document
	pdflatex -halt-on-error -output-directory ${TEMPDIR} "$1" || exit 1
	pdflatex -halt-on-error -output-directory ${TEMPDIR} "$1" || exit 1
fi

# Determine filename of generated .pdf file
PDFILENAMEBASE="$(basename "$1")"
PDFILENAMEBASE="${PDFILENAMEBASE/.tex/.pdf}"
PDFILENAME="${TEMPDIR}/${PDFILENAMEBASE}"

if [[ ! -f "${PDFILENAME}" ]] ; then
	echo "File '${PDFILENAME}' does not exist" >&2
	exit 1
fi

# Copy generated .pdf file next to original .tex file
cp -p "${PDFILENAME}" "$(dirname "$1")" || exit 1

/* Copyright (c) 2018-2020, Thomas Fischer <fischer@unix-ag.uni-kl.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Thomas Fischer nor the names of other contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *binary = "01";
static const unsigned int mod_for_binary = 2;
static const size_t bits_for_binary = 1;
static const char *numbers = "0123456789";
static const unsigned int mod_for_numbers = 10;
static const size_t bits_for_numbers = 4;
static const char *luhn = "0123456789";
static const unsigned int mod_for_luhn = 10;
static const size_t bits_for_luhn = 4;
static const char *hex = "0123456789ABCDEF";
static const unsigned int mod_for_hex = 16;
static const size_t bits_for_hex = 4;
static const char *uuid = "0123456789abcdef";
static const unsigned int mod_for_uuid = 16;
static const size_t bits_for_uuid = 4;
static const char *maceui48 = "0123456789ABCDEF";
static const unsigned int mod_for_maceui48 = 16;
static const size_t bits_for_maceui48 = 4;
static const char *alpha = "qaywsxedcrfvtgbzhnujmikolpQAYWSXEDCRFVTGBZHNUJMIKOLP";
static const unsigned int mod_for_alpha = 52;
static const size_t bits_for_alpha = 6;
static const char *alphanum = "0123456789qaywsxedcrfvtgbzhnujmikolpQAYWSXEDCRFVTGBZHNUJMIKOLP";
static const unsigned int mod_for_alphanum = 62;
static const size_t bits_for_alphanum = 6;
static const char *alphanumspecial = "0123456789qaywsxedcrfvtgbzhnujmikolpQAYWSXEDCRFVTGBZHNUJMIKOLP!\"#%&/()=+?^*'-_.,;<>{}[]";
static const unsigned int mod_for_alphanumspecial = 87;
static const size_t bits_for_alphanumspecial = 7;
static const char *keyboardsafe = "0123456789qawsxedcrfvtgbhnujmikolpQAWSXEDCRFVTGBHNUJMIKOLP!%.,";
static const unsigned int mod_for_keyboardsafe = 62;
static const size_t bits_for_keyboardsafe = 6;

static const unsigned int ERROR_RANDOM_BITS = 0xffff;

int num_characters = 8, count_passwords = 1;

enum CharSet {Binary, Numbers, Luhn, Hex, UUID, MacEui48, Alpha, AlphaNum, AlphaNumSpecial, KeyboardSafe};
enum CharSet charset = KeyboardSafe;
enum CaseChange {NoCaseChange, ToUpperCase, ToLowerCase};
enum CaseChange caseChange = NoCaseChange;

void print_version(FILE *output)
{
    fprintf(output, "makepasswd v0.4\nCopyright 2018-2020 by Thomas Fischer <fischer@unix-ag.uni-kl.de>\n");
}

/**
 * Print help on how to use this program.
 * Output is written to a file device as specified by
 * the caller, usually 'stdout' or 'stderr'.
 */
void print_help(FILE *output)
{
    print_version(output);
    fprintf(output, "\nUsage:  makepasswd [--help] [--length N] [--binary|--num|--luhn|--alpha|\n");
    fprintf(output, "                    --alphanum|--special|--keyboard|--weak|--medium|--strong]\n");
    fprintf(output, " --help      Print this help and exit without computing a password\n");
    fprintf(output, " --version   Print version and copyright information\n");
    fprintf(output, " --length N  Computed password shall be N characters long (default: N=%i)\n", num_characters);
    fprintf(output, " --count N   Number of passwords to be generated; one per line will be\n");
    fprintf(output, "             printed (default: N=%i)\n", count_passwords);
    fprintf(output, " --binary    Computed password shall contain only digits 0 and 1\n");
    fprintf(output, " --num       Computed password shall contain only digits 0 to 9\n");
    fprintf(output, " --luhn      Computed a Luhn number, i.e. where last digit is the checksum\n");
    fprintf(output, " --hex       Computed password shall contain only digits 0 to 9\n");
    fprintf(output, "             and letters A to F (only upper case)\n");
    fprintf(output, " --uuid      Computed something that looks like an UUID, but is not\n");
    fprintf(output, "             conforming to RFC 4122\n");
    fprintf(output, " --mac       Create a random unicast, locally administered EUI-48 MAC address\n");
    fprintf(output, " --alpha     Computed password shall contain only letters A to Z\n");
    fprintf(output, "             (both upper and lower case)\n");
    fprintf(output, " --alphanum  Computed password shall contain only digits 0 to 9\n");
    fprintf(output, "             and letters A to Z (both upper and lower case)\n");
    fprintf(output, " --special   Computed password shall contain only digits 0 to 9,\n");
    fprintf(output, "             letters A to Z (both upper and lower case),\n");
    fprintf(output, "             and selected special characters like ! or #\n");
    fprintf(output, " --keyboard  Computed password shall contain only characters\n");
    fprintf(output, "             that have the same position on English, German,\n");
    fprintf(output, "             and Swedish keyboard layouts, such as 1, g, and !\n");
    fprintf(output, " --weak      Apply default settings to generate a weak password\n");
    fprintf(output, " --medium    Apply default settings to generate a medium password\n");
    fprintf(output, " --strong    Apply default settings to generate a strong password\n");
    fprintf(output, " --lc        Make all letters lower case\n");
    fprintf(output, " --uc        Make all letters upper case\n");
    fprintf(output, "\nArguments --binary, --num, --luhn, --hex, --uuid, --mac, --alpha, --special,\n");
    fprintf(output, "          --keyboard, --weak, --medium, and --strong are mutually exclusive.\n");
    fprintf(output, "Default password mode is  %s.\n", (charset == Binary ? "--binary" : (charset == Numbers ? "--num" : (charset == Hex ? "--hex" : (charset == Alpha ? "--alpha" : (charset == AlphaNum ? "--alphanum" : (charset == AlphaNumSpecial ? "--special" : (charset == KeyboardSafe ? "--keyboard" : "unset"))))))));
}

/**
 * Read a limited number of random bits from /dev/urandom
 * and return them as the LSB of an unsigned int.
 * The parameter 'queried_bits's value must be less than
 * the number of bits an unsigned int can hold.
 */
unsigned int get_random_bits(const size_t queried_bits)
{
    /// Check input arguments for sanity
    if (queried_bits >= (sizeof(unsigned int) * 8)) return ERROR_RANDOM_BITS;

    /// Declare and initialize internal buffer of random bits
    static unsigned int buffered_random_bits = 0;
    static size_t remaining_bits = 0;

    /// Check if more random bits need to be retrieved from a random device
    if (remaining_bits < queried_bits) {
        FILE *devrandom = fopen("/dev/urandom", "r");
        if (devrandom != NULL) {
            while (remaining_bits < queried_bits) {
                unsigned short new_random_bytes = 0;
                if (fread(&new_random_bytes, sizeof(unsigned short), 1, devrandom) != 1) break;
                buffered_random_bits <<= (sizeof(unsigned short)) * 8;
                buffered_random_bits |= new_random_bytes;
                remaining_bits += (sizeof(unsigned short)) * 8;
            }
            fclose(devrandom);
        }
    }

    if (remaining_bits >= queried_bits) {
        /// Extract result from buffered random bits using a mask
        unsigned int resulting_bits = buffered_random_bits & ((1 << queried_bits) - 1);
        /// Remove extracted bits from static buffer via shift left
        buffered_random_bits >>= queried_bits;
        remaining_bits -= queried_bits;
        /// Return result
        return resulting_bits;
    } else
        return ERROR_RANDOM_BITS;
}

int main(int argc, char *argv[])
{
    /// Process command line options, overwriting default values
    for (int i = 1; i < argc; ++i) {
        if (i < argc - 1 && strlen(argv[i]) == 8 && strncmp(argv[i], "--length", 8) == 0) {
            num_characters = atoi(argv[i + 1]);
            ++i;
        } else if (strlen(argv[i]) > 9 && strncmp(argv[i], "--length=", 9) == 0)
            num_characters = atoi(argv[i] + 9);
        else if (i < argc - 1 && strlen(argv[i]) == 7 && strncmp(argv[i], "--count", 7) == 0) {
            count_passwords = atoi(argv[i + 1]);
            ++i;
        } else if (strlen(argv[i]) > 8 && strncmp(argv[i], "--count=", 8) == 0)
            count_passwords = atoi(argv[i] + 8);
        else if (strncmp(argv[i], "--binary", 8) == 0)
            charset = Binary;
        else if (strncmp(argv[i], "--num", 5) == 0)
            charset = Numbers;
        else if (strncmp(argv[i], "--luhn", 6) == 0)
            charset = Luhn;
        else if (strncmp(argv[i], "--hex", 5) == 0)
            charset = Hex;
        else if (strncmp(argv[i], "--uuid", 6) == 0)
            charset = UUID;
        else if (strncmp(argv[i], "--mac", 5) == 0)
            charset = MacEui48;
        else if (argv[i][7] == '\0' && strncmp(argv[i], "--alpha", 7) == 0) ///< need to test for zero byte at pos 7 to avoid matching '--alphanum'
            charset = Alpha;
        else if (strncmp(argv[i], "--alphanum", 10) == 0)
            charset = AlphaNum;
        else if (strncmp(argv[i], "--special", 9) == 0)
            charset = AlphaNumSpecial;
        else if (strncmp(argv[i], "--keyboard", 10) == 0)
            charset = KeyboardSafe;
        else if (strncmp(argv[i], "--weak", 6) == 0) {
            charset = Alpha;
            num_characters = 6;
        } else if (strncmp(argv[i], "--medium", 8) == 0) {
            charset = AlphaNum;
            num_characters = 10;
        } else if (strncmp(argv[i], "--strong", 8) == 0) {
            charset = AlphaNumSpecial;
            num_characters = 16;
        } else if (strncmp(argv[i], "--lc", 4) == 0) {
            caseChange = ToLowerCase;
        } else if (strncmp(argv[i], "--uc", 4) == 0) {
            caseChange = ToUpperCase;
        } else if (strncmp(argv[i], "--help", 6) == 0) {
            print_help(stdout);
            return 1;
        } else if (strncmp(argv[i], "--version", 9) == 0) {
            print_version(stdout);
            return 1;
        } else {
            fprintf(stderr, "Unknown or incomplete argument: %s\n", argv[i]);
            return 1;
        }
    }

    if (charset == UUID)
        num_characters = 32; ///< UUIDs have fixed length
    else if (charset == MacEui48)
        num_characters = 12; ///< EUI-48 addresses have fixed length
    if (num_characters <= 0 || num_characters > 512 || (num_characters < 2 && charset == Luhn)) {
        fprintf(stderr, "Invalid number of characters given\n");
        return 1;
    } else if (count_passwords <= 0 || count_passwords > 512) {
        fprintf(stderr, "Invalid count of passwords given\n");
        return 1;
    }

    const size_t bits = charset == Binary ? bits_for_binary : (charset == Numbers ? bits_for_numbers : (charset == Luhn ? bits_for_luhn : (charset == Hex ? bits_for_hex : (charset == UUID ? bits_for_uuid: (charset == MacEui48 ? bits_for_maceui48 : (charset == Alpha ? bits_for_alpha : (charset == AlphaNum ? bits_for_alphanum : (charset == AlphaNumSpecial ? bits_for_alphanumspecial : bits_for_keyboardsafe))))))));
    const unsigned int mod = charset == Binary ? mod_for_binary : (charset == Numbers ? mod_for_numbers : (charset == Luhn ? mod_for_luhn : (charset == Hex ? mod_for_hex : (charset == UUID ? mod_for_uuid : (charset == MacEui48 ? mod_for_maceui48 : (charset == Alpha ? mod_for_alpha : (charset == AlphaNum ? mod_for_alphanum : (charset == AlphaNumSpecial ? mod_for_alphanumspecial : mod_for_keyboardsafe))))))));
    const char *chars = charset == Binary ? binary : (charset == Numbers ? numbers : (charset == Luhn ? luhn : (charset == Hex ? hex : (charset == UUID ? uuid : (charset == MacEui48 ? maceui48 : (charset == Alpha ? alpha : (charset == AlphaNum ? alphanum : (charset == AlphaNumSpecial ? alphanumspecial : keyboardsafe))))))));
    if (charset == Luhn) --num_characters; ///< because the last digit will be computed
    for (int k = 0; k < count_passwords; ++k) {
        int luhn_sum = 0;
        for (int i = 0; i < num_characters; ++i) {
            const unsigned int r = get_random_bits(bits);
            if (r == ERROR_RANDOM_BITS) break;
            char c = chars[r % mod];
            if (charset == MacEui48 && i == 1) {
                // For unicast, locally administered EUI-48 MAC address, the first byte's lower nibble must be either 2, 6, A, or E
                static char digitsForLocalMACranges[] = "26AE";
                c = digitsForLocalMACranges[r % 4];
            }
            if (caseChange == ToLowerCase && c >= 'A' && c <= 'Z') c += 0x20;
            else if (caseChange == ToUpperCase && c >= 'a' && c <= 'z') c -= 0x20;
            if (charset == Luhn) {
                const int doubledigit = ((num_characters - i) % 2 + 1) * (c - '0');
                luhn_sum += doubledigit % 10 + doubledigit / 10;
            } else if (charset == UUID) {
                if (i == 8 || i == 12 || i == 16 || i == 20)
                    putc('-', stdout);
            } else if (charset == MacEui48) {
                if (i > 0 && i % 2 == 0)
                    putc(':', stdout);
            }
            printf("%c", c);
        }
        if (charset == Luhn)
            printf("%c", ((luhn_sum * 9) % 10) + '0');
        printf("\n");
    }

    return 0;
}

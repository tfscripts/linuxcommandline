#!/usr/bin/env bash

# Copyright (c) 2014, Thomas Fischer <fischer@unix-ag.uni-kl.de>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#    Neither the name of Thomas Fischer nor the names of other contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This script requires external programs available through
# https://git.gitorious.org/tfscripts/linuxcommandline.git
# (1) randline
# (2) queue

declare -a SITELIST=()
SET_VERBOSE=0
FETCH_JPEG=greedy
FETCH_PNG=greedy
FETCH_GIF=all
FETCH_VIDEO=none
PRINT_HELP=0
PAGE_FIRST=1
PAGE_LAST=160
MOZILLAVERSION=$(( ${RANDOM} % 13 + 22 ))
MOZILLAMINORVERSION=$(( ${RANDOM} % 3 ))
YEAR=$(( $(date '+%Y') - ( ${RANDOM} % 5 ) ))
MONTH=$(( ${RANDOM} % 12 + 1 ))
DAY=$(( ${RANDOM} % 30 + 1 ))
DATE=$(printf "%04d%02d%02d" $YEAR $MONTH $DAY)
USER_AGENT="Mozilla/5.0 (X11; Linux i686; rv:${MOZILLAVERSION}.${MOZILLAMINORVERSION}) Gecko/${DATE} Firefox/${MOZILLAVERSION}.${MOZILLAMINORVERSION}"
RANDLINE_ARGS=-q
QUEUE_ARGS=-s
LIMIT_IMAGES=32768
FLAG_LOOKINTOPOSTINGS=0

function is_positive_integer {
	printf "%d" $1 > /dev/null 2>&1 && test $1 -gt 0
	return $?
}

function parsearguments {
	local -a RAWSITELIST=()
	local ARGS=$(getopt -o "h" -l "help,verbose,jpeg:,jpg:,png:,gif:,video:,first:,last:,limit-images:,look-into-postings:" -n $(basename $0) -- "$@")
	
	#Bad arguments
	if [ $? -ne 0 ] ; then
		exit 1
	fi

	# A little magic
	eval set -- "$ARGS"

	while true ; do
		case "$1" in
		-h|--help)
			PRINT_HELP=1
			shift
		;;
		--verbose )
			SET_VERBOSE=1
			RANDLINE_ARGS=
			QUEUE_ARGS="-V -V"
			shift
		;;
		--jpeg|--jpg )
			if [[ $2 = "no" || $2 = "none" || $f = "false" ]] ; then
				FETCH_JPEG=none
			elif [[ $2 = "yes" || $2 = "all" || $2 = "true" ]] ; then
				FETCH_JPEG=all
			elif [[ $2 = "large" || $2 = "big" ]] ; then
				FETCH_JPEG=large
			elif [[ $2 = "greedy" ]] ; then
				FETCH_JPEG=greedy
			else
				echo "Invalid argument for parameter \"--jpeg\": \"$2\"" >&2
				exit 1
			fi
			shift 2;;
		--png )
			if [[ $2 = "no" || $2 = "none" || $f = "false" ]] ; then
				FETCH_PNG=none
			elif [[ $2 = "yes" || $2 = "all" || $2 = "true" ]] ; then
				FETCH_PNG=all
			elif [[ $2 = "large" || $2 = "big" ]] ; then
				FETCH_PNG=large
			elif [[ $2 = "greedy" ]] ; then
				FETCH_PNG=greedy
			else
				echo "Invalid argument for parameter \"$1\": \"$2\"" >&2
				exit 1
			fi
			shift 2;;
		--gif )
			if [[ $2 = "no" || $2 = "none" || $f = "false" ]] ; then
				FETCH_GIF=none
			elif [[ $2 = "yes" || $2 = "all" || $2 = "true" ]] ; then
				FETCH_GIF=all
			else
				echo "Invalid argument for parameter \"$1\": \"$2\"" >&2
				exit 1
			fi
			shift 2;;
		--video )
			if [[ $2 = "no" || $2 = "none" || $f = "false" ]] ; then
				FETCH_VIDEO=none
			elif [[ $2 = "tumblr" ]] ; then
				FETCH_VIDEO=tumblr
			elif [[ $2 = "yes" || $2 = "all" || $2 = "true" ]] ; then
				FETCH_VIDEO=all
			else
				echo "Invalid argument for parameter \"$1\": \"$2\"" >&2
				exit 1
			fi
			shift 2;;
		--first )
			if ! is_positive_integer $2 ; then
				echo "Invalid argument for parameter \"$1\": \"$2\"" >&2
				exit 1
			fi
			PAGE_FIRST=$2
			shift 2;;
		--last )
			if ! is_positive_integer $2 ; then
				echo "Invalid argument for parameter \"$1\": \"$2\"" >&2
				exit 1
			fi
			PAGE_LAST=$2
			shift 2;;
		--limit-images )
			if ! is_positive_integer $2 ; then
				echo "Invalid argument for parameter \"$1\": \"$2\"" >&2
				exit 1
			fi
			LIMIT_IMAGES=$2
			shift 2;;
		--look-into-postings)
			if [[ $2 = "no" || $2 = "false" ]] ; then
				FLAG_LOOKINTOPOSTINGS=0
			elif [[ $2 = "yes" || $2 = "true" ]] ; then
				FLAG_LOOKINTOPOSTINGS=1
			else
				echo "Invalid argument for parameter \"$1\": \"$2\"" >&2
				exit 1
			fi
			shift 2;;
		--)
			shift
			break
		esac
	done

	if [[ ${PRINT_HELP} -ne 0 ]] ; then
		echo "Usage: $(basename $0) [ options ] sitename1 [ sitename2 [ sitename3 [ ... ] ] ]" >&2
		echo "Downloads all (most) images from a given tumblr site to the local directory." >&2
		echo >&2
		echo "Options:" >&2
		echo " --help" >&2
		echo "    Show this help" >&2
		echo " --verbose" >&2
		echo "    Show verbose (more) output and progress information" >&2
		echo " --jpeg=(none|large|greedy|all)" >&2
		echo "    none   = do not download JPEG files" >&2
		echo "    large  = download only high-res JPEG files" >&2
		echo "    greedy = based on low-res JPEG files, guess high-res version" >&2
		echo "    all    = download all JPEG files" >&2
		echo "    (default: ${FETCH_JPEG})">&2
		echo " --png=(none|large|greedy|all)" >&2
		echo "    none   = do not download PNG files" >&2
		echo "    large  = download only high-res PNG files" >&2
		echo "    greedy = based on low-res PNG files, guess high-res version" >&2
		echo "    all    = download all PNG files" >&2
		echo "    (default: ${FETCH_PNG})">&2
		echo " --gif=(none|all)" >&2
		echo "    none   = do not download GIF files" >&2
		echo "    all    = download all GIF files" >&2
		echo "    (default: ${FETCH_GIF})">&2
		echo " --video=(none|tumblr|all)" >&2
		echo "    none   = do not download video files" >&2
		echo "    tumblr = download video files at tumblr.com" >&2
		echo "    all    = download all video files, including YouTube" >&2
		echo "    (default: ${FETCH_VIDEO})">&2
		echo " --first=n">&2
		echo "    n      = index of first page (default: ${PAGE_FIRST})">&2
		echo " --last=n">&2
		echo "    n      = index of last page (default: ${PAGE_LAST})">&2
		echo " --limit-images=n">&2
		echo "    n      = limit the number of downloaded images (default: ${LIMIT_IMAGES})">&2
		echo " --look-into-postings=(yes|no)">&2
		echo "    whether to look into individual posting pages to find images">&2
		STRING=${FLAG_LOOKINTOPOSTINGS/1/yes}
		STRING=${STRING/0/no}
		echo "    (default: ${STRING})">&2
		exit 1
	fi

	if [[ $# -eq 0 ]] ; then
		echo "Require at least one sitename as argument" >&2
		exit 1
	fi

	while test -n "$1" ; do
		local site="$1"
		if [[ ${site} != "${site/.tumblr.com/}" ]] ; then
			site=$(grep -Po --color=NEVER -i '[a-z0-9]+[.]tumblr[.]com' <<<${site} | grep -vE '\b(media|www)[.]tumblr[.]com' | head -n 1 | sed -e 's/[.]tumblr[.]com//')
		fi
		RAWSITELIST+=("${site}")
		shift
	done
	
	# sort site list to have unique values only
	readarray -t SITELIST < <(for site in "${RAWSITELIST[@]}" ; do echo "${site}" ; done | sort -u)

	if [[ ${PAGE_FIRST} -gt ${PAGE_LAST} ]] ; then
		echo "First page has to be smaller or equal to last page" >&2
		exit 1
	fi

	echo "Parameters:"
	echo "  SET_VERBOSE=${SET_VERBOSE}"
	echo "    RANDLINE_ARGS=${RANDLINE_ARGS}"
	echo "    QUEUE_ARGS=${QUEUE_ARGS}"
	echo "  FETCH_JPEG=${FETCH_JPEG}"
	echo "  FETCH_PNG=${FETCH_PNG}"
	echo "  FETCH_GIF=${FETCH_GIF}"
	echo "  FETCH_VIDEO=${FETCH_VIDEO}"
	echo "  PAGE_FIRST=${PAGE_FIRST}"
	echo "  PAGE_LAST=${PAGE_LAST}"
	echo "  USER_AGENT=${USER_AGENT}"
	echo "  SITELIST=${SITELIST[@]}"
	echo "  LIMIT_IMAGES=${LIMIT_IMAGES}"
	STRING=${FLAG_LOOKINTOPOSTINGS/1/yes}
	STRING=${STRING/0/no}
	echo "  FLAG_LOOKINTOPOSTINGS=${STRING}"
}

function settitle {
	local title="$1"
	echo -ne "\033]0;${title}\007"
}

function downloadsite {
	local site="$1"

	baseurl="http://${site}.tumblr.com"

	tempdirname=$(mktemp -d)
	imageurls=$(mktemp)
	videourls=$(mktemp)
	youtubeurls=$(mktemp)
	queuefilename=$(mktemp)


	echo
	settitle "[tumblr] ${site}: Downloading pages"
	echo "${site}: Downloading pages ===-------------------------------"
	echo

	for (( n=${PAGE_FIRST} ; n<=${PAGE_LAST} ; ++n )) ; do
		url="${baseurl}/page/${n}"
		htmlfilename="${tempdirname}/${n}.html"
		echo "wget --limit-rate=48k --user-agent=\"${USER_AGENT}\" -T 30 \"${url}\" -O \"${htmlfilename}\""
	done | randline ${RANDLINE_ARGS} >${queuefilename}

	queue -p ${tempdirname} ${QUEUE_ARGS} -j 11 ${queuefilename}
	rm -f ${queuefilename}

	grep -Poh --color=NEVER 'http[^"]+/photoset_iframe/[^"]+' ${tempdirname}/*.html | sort -u | randline ${RANDLINE_ARGS} | while read iframeurl ; do
		echo "wget --limit-rate=48k --user-agent=\"${USER_AGENT}\" -T 30 \"${iframeurl}\" -O \"${tempdirname}/iframe-${RANDOM}-${RANDOM}.html\""
	done | randline ${RANDLINE_ARGS} >${queuefilename}

	queue -p ${tempdirname} ${QUEUE_ARGS} -j 11 ${queuefilename}
	rm -f ${queuefilename}

	if [ ${FLAG_LOOKINTOPOSTINGS} -eq 1 ] ; then
		grep -Poh --color=NEVER 'http[^"]+[.]tumblr[.]com/post/[0-9/]+' ${tempdirname}/*.html | sort -u | randline -q | while read posturl ; do
			echo "wget --limit-rate=48k --user-agent=\"${USER_AGENT}\" -T 30 \"${posturl}\" -O \"${tempdirname}/post-${RANDOM}-${RANDOM}.html\""
		done | randline -q >${queuefilename}

		queue -p ${tempdirname} -V -j 11 ${queuefilename}
		rm -f ${queuefilename}
	fi

	echo
	settitle "[tumblr] ${site}: Downloading images"
	echo "${site}: Downloading images ===-------------------------------"
	echo

	grep -Poh --color=NEVER 'https?://[0-9]+.media.tumblr.com/([0-9a-f]+/)?tumblr_[^"]+[.](gif|jpe?g?|png)\b' ${tempdirname}/*.html >${imageurls}
	# include links to images at imgur
	grep -Poh --color=NEVER 'https?://i[.]imgur[.].com/[^"]+[.](gif|jpe?g?|png)\b' ${tempdirname}/*.html >>${imageurls}

	test "${FETCH_JPEG}" = "none" && cp -p ${imageurls} ${imageurls}2 && grep --color=NEVER -v -P '[.]jpe?g?\b' ${imageurls}2 >${imageurls}
	test "${FETCH_PNG}" = "none" && cp -p ${imageurls} ${imageurls}2 && grep --color=NEVER -v -P '[.]png\b' ${imageurls}2 >${imageurls}
	test "${FETCH_JPEG}" = "large" && cp -p ${imageurls} ${imageurls}2 && grep --color=NEVER -v -P '_\d{1,3}[.]jpe?g?\b' ${imageurls}2 >${imageurls}
	test "${FETCH_JPEG}" = "greedy" && cp -p ${imageurls} ${imageurls}2 && sed -e 's/_[0-9][0-9][0-9].jpe?g?/_1280.jpe?g?/g' <${imageurls}2 >${imageurls}
	test "${FETCH_PNG}" = "large" && cp -p ${imageurls} ${imageurls}2 && grep --color=NEVER -v -P '_\d{1,3}[.]png\b' ${imageurls}2 >${imageurls}
	test "${FETCH_PNG}" = "greedy" && cp -p ${imageurls} ${imageurls}2 && sed -e 's/_[0-9][0-9][0-9].png/_1280.png/g' <${imageurls}2 >${imageurls}
	test "${FETCH_GIF}" = "none" && cp -p ${imageurls} ${imageurls}2 && grep --color=NEVER -v -P '[.]gif\b' ${imageurls}2 >${imageurls}
	rm -f ${imageurls}2
	test "${FETCH_VIDEO}" = "all" && grep -Poh --color=NEVER 'https?://[^.]+.tumblr.com/video_file/[0-9]+/[^" \\]+' ${tempdirname}/*.html >${videourls}
	test "${FETCH_VIDEO}" = "all" && grep -Poh --color=NEVER 'https?://www.youtube.com/embed/[^ "?#\\]+' ${tempdirname}/*.html >${youtubeurls}
	test "${FETCH_VIDEO}" = "tumblr" && grep -Poh --color=NEVER 'https?://[^.]+.tumblr.com/video_file/[0-9]+/[^" \\]+' ${tempdirname}/*.html >>${videourls}

	if [[ $(wc -l <${imageurls}) -eq 0 && $(wc -l <${videourls}) -eq 0 && $(wc -l <${youtubeurls}) -eq 0 ]] ; then
		echo "No images or videos found for site \"${site}\", skipping this site" >&2
		settitle ""
		rm -rf ${tempdirname} ${imageurls} ${videourls} ${youtubeurls} ${queuefilename}
		return
	fi

	rm -rf ${tempdirname} ; mkdir ${tempdirname}
	sort -u <${imageurls} | randline ${RANDLINE_ARGS} | while read imageurl ; do
		echo "wget --limit-rate=48k --user-agent=\"${USER_AGENT}\" -T 30 -c \"${imageurl}\""
	done >${queuefilename}
	rm -f ${imageurls}

	sort -u <${videourls} | randline ${RANDLINE_ARGS} | while read videourl ; do
		filename="${site}-${RANDOM}-${RANDOM}.mp4"
		echo "wget --limit-rate=64k --user-agent=\"${USER_AGENT}\" -T 30 -c -O \"${filename}\" \"${videourl}\""
	done >>${queuefilename}

	sort -u <${youtubeurls} | randline ${RANDLINE_ARGS} | while read youtubeurl ; do
		echo "youtube-dl -c \"${youtubeurl}\""
	done >>${queuefilename}

	cp -p ${queuefilename} ${queuefilename}2 && randline -n ${LIMIT_IMAGES} <${queuefilename}2 >${queuefilename} ; rm -f ${queuefilename}2
	queue -p ${tempdirname} ${QUEUE_ARGS} -j 11 ${queuefilename}
	rm -f ${queuefilename}

	rm -rf ${tempdirname}

	settitle ""
}

parsearguments "$@"

for site in "${SITELIST[@]}" ; do
	downloadsite "${site}"
done


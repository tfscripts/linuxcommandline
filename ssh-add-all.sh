#!/usr/bin/env bash

#  Copyright (c) 2015-2022, Thomas Fischer <fischer@unix-ag.uni-kl.de>
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#     *  Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#     *  Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#     *  Neither the name of Thomas Fischer nor the names of other contributors
#        may be used to endorse or promote products derived from this software
#        without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
#  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This script will go through all private/public key pairs in your home
# directory's .ssh/ directory and call 'ssh-add' for those keys.
# By default, 'ksshaskpass' will be used to retrieve the ssh key's
# passphrase; this passphrase will be stored and read from KDE's KWallet.
# This script has been written to run with 'dash' (Debian Almquist shell),
# but should be compatible to be run with a standard Linux bash.

# Required for this script to run is 'ksshaskpass':
#  https://invent.kde.org/plasma/ksshaskpass
# If you don't like it, set another program's path to SSH_ASKPASS

# Installation:
# 1. Make adjustments for paths or commands if necessary
# 2. Test script on command line if it works as intended,
#    i.e. if 'ssh-add' is called on all your private SSH keys
# 3. Copy it to a directory which is in your PATH

find -L "${HOME}"/.ssh/ -type f -name 'id_*.pub' | sed -e 's/[.]pub$//g' | while read -r privatekeyfile ; do test -f "${privatekeyfile}" -a -O "${privatekeyfile}" || continue ; echo "Adding private key '${privatekeyfile}'" ; SSH_ASKPASS="$(which ksshaskpass)" ssh-add "${privatekeyfile}" </dev/null || exit 1 ; done

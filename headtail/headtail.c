/* Copyright (c) 2015, Thomas Fischer <fischer@unix-ag.uni-kl.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Thomas Fischer nor the names of other contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LINES (1024*1024*2)
#define MAX_LINE_LENGTH 1024

int read_lines(FILE *input, int readnull, char **lines)
{
    char buffer[ MAX_LINE_LENGTH ];
    int inbyte;
    int strpos;
    int i;

    if (readnull) {
        strpos = 0;
        i = 0;
        inbyte = fgetc(input);
        while (inbyte != EOF) {
            buffer[ strpos ] = (unsigned char) inbyte;
            strpos++;

            if (inbyte == '\0' || strpos >= MAX_LINE_LENGTH - 1) {
                buffer[ strpos ] = '\0';
                lines[ i ] = strdup(buffer);
                i++;
                strpos = 0;
                if (i >= MAX_LINES)
                    break;
            }
            if (feof(input))
                break;
            inbyte = fgetc(input);
        }
    } else {
        for (i = 0; (i < MAX_LINES) && !feof(input);) {
            if (fgets(buffer, MAX_LINE_LENGTH, input) != NULL) {
                lines[ i ] = strdup(buffer);
                ++i;
            }
        }
    }

    for (int j = i; j < MAX_LINES; ++j)
        lines[ j ] = NULL;

    return i;
}

int head_tail_file(int head_count, int tail_count, int readnull, FILE *input)
{
    char **lines = (char **)malloc(sizeof(char *) * MAX_LINES);
    int linecount = read_lines(input, readnull, lines);

    if (linecount <= head_count + tail_count) {
        /// print all lines
        for (int i = 0; i < linecount; i++) {
            printf("%s", lines[i]);
            if (readnull)
                fputc('\0', stdout);
        }
    } else {
        /// print head first
        for (int i = 0; i < head_count; i++) {
            printf("%s", lines[i]);
            if (readnull)
                fputc('\0', stdout);
        }
        /// print separator
        if (!readnull)
            printf(">>> Skipping %d lines <<<\n", linecount - head_count - tail_count);
        /// print tail last
        for (int i = 0, ln = linecount - tail_count; i < tail_count; i++, ++ln) {
            printf("%s", lines[ln]);
            if (readnull)
                fputc('\0', stdout);
        }
    }

    for (int i = 0; i < linecount + 1; i++)
        free(lines[ i ]);
    free(lines);

    return linecount;
}

int main(int argc, char *argv[])
{
    int head_count = 10;
    int tail_count = 10;
    int readnull = 0;
    int number_files = 0;
    int done_arguments = 0;

    for (int i = 1; i < argc; ++i) {
        if (argv[ i ][ 0 ] == '-' && done_arguments == 0) {
            if (argv[ i ][ 1 ] == 'n' && argv[ i ][ 2 ] == 0 && i < argc - 1) {
                ++i;
                if (argv[ i ][ 0 ] >= '1' && argv[ i ][ 0 ] <= '9')
                    head_count = tail_count = atoi(argv[ i ]);
            } else if (argv[ i ][ 1 ] == 'n' && argv[ i ][ 2 ] >= '1' && argv[ i ][ 2 ] <= '9') {
                head_count = tail_count = atoi(argv[ i ] + 2);
            } else if (argv[ i ][ 1 ] == 'h' && argv[ i ][ 2 ] == 0 && i < argc - 1) {
                ++i;
                if (argv[ i ][ 0 ] >= '1' && argv[ i ][ 0 ] <= '9')
                    head_count = atoi(argv[ i ]);
            } else if (argv[ i ][ 1 ] == 'h' && argv[ i ][ 2 ] >= '1' && argv[ i ][ 2 ] <= '9') {
                head_count = atoi(argv[ i ] + 2);
            } else if (argv[ i ][ 1 ] == 't' && argv[ i ][ 2 ] == 0 && i < argc - 1) {
                ++i;
                if (argv[ i ][ 0 ] >= '1' && argv[ i ][ 0 ] <= '9')
                    tail_count = atoi(argv[ i ]);
            } else if (argv[ i ][ 1 ] == 't' && argv[ i ][ 2 ] >= '1' && argv[ i ][ 2 ] <= '9') {
                tail_count = atoi(argv[ i ] + 2);
            } else if ((argv[ i ][ 1 ] == '0' || argv[ i ][ 1 ] == 'Z') && argv[ i ][ 2 ] == 0)
                readnull = 1;
            else if (argv[i][1] == '-' && argv[i][2] == 0)
                done_arguments = 1;
            else if ((argv[ i ][ 1 ] == 'h' && argv[ i ][ 2 ] == 0) || strcmp(argv[ i ], "--help") == 0) {
                fprintf(stderr, "Copyright (c) 2015 Thomas Fischer.\nAll rights reserved.\n\n");
                fprintf(stderr, "Get the source code at:\n  https://gitorious.org/tfscripts/linuxcommandline\nReleased under the three-clause BSD license\n\n");
                fprintf(stderr, "Combines the features of 'head' and 'tail'\n\n");
                fprintf(stderr, "Usage: headtail [-n N] [-h N] [-t N] [file1 [file2 ...] ]\n");
                fprintf(stderr, "  -n N   Number of lines to print for both head and tail\n");
                fprintf(stderr, "  -h N   Number of lines to print for head\n");
                fprintf(stderr, "  -t N   Number of lines to print for tail\n");
                fprintf(stderr, "  -0     Read and write zero-terminated lines of text\n");
                fprintf(stderr, "  -Z     Same as -0, included for compatibility\n");
                fprintf(stderr, "\nBy default, 10 lines are printed for both head and tail.\n");
                fprintf(stderr, "For multiple occurrences of  '-n', '-h', and '-t', the order of arguments counts.\n");
                fprintf(stderr, "If no file names are supplied, lines of text are read from stdin.\n");
                return 1;
            }
        } else {
            FILE *input = fopen(argv[i], "r");
            if (readnull == 0)
                printf("=== %s ===\n", argv[i]);
            if (input != NULL) {
                head_tail_file(head_count, tail_count, readnull, input);
                fclose(input);
            }
            ++number_files;
        }
    }

    if (number_files == 0) {
        /// no files provided in argument list? read from stdin
        head_tail_file(head_count, tail_count, readnull, stdin);
    }

    return 0;
}

/*

Copyright (c) 2010-2014 Thomas Fischer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include <search.h>

#define MAX_LINES (1024*1024*2)
#define MAX_LINE_LENGTH (1024*64)

#define HASHTABSIZE 16384
struct hsearch_data *hash_table;

void add_to_hash_table(const char *line){
	ENTRY e, *ret;
	e.key = strdup(line);
	e.data = NULL;
	hsearch_r(e, ENTER, &ret, hash_table);
}

int find_in_hash_table(const char *line) {
	ENTRY e, *ret;
	e.key = strdup(line);
	e.data = NULL;
	return hsearch_r(e, FIND, &ret, hash_table);
}

int read_lines(int readnull, char **lines, int unique)
{
	char buffer[ MAX_LINE_LENGTH ];
	int inbyte;
	int strpos;
	int i;

	if(readnull) {
		strpos = 0;
		i = 0;
		inbyte = fgetc(stdin);
		while(inbyte != EOF) {
			buffer[ strpos ] = (unsigned char) inbyte;
			strpos++;

			if(inbyte == '\0' || strpos >= MAX_LINE_LENGTH - 1) {
				buffer[ strpos ] = '\0';
				if (unique == 0 || find_in_hash_table(buffer) == 0) {
					lines[ i ] = strdup(buffer);
					i++;
					if (unique == 1)
						add_to_hash_table(buffer);
				}
				strpos = 0;
				if(i >= MAX_LINES)
					break;
			}
			if(feof(stdin))
				break;
			inbyte = fgetc(stdin);
		}
	} else {
		for(i = 0; (i < MAX_LINES) && !feof(stdin);) {
			if(fgets(buffer, MAX_LINE_LENGTH, stdin) != NULL) {
				if (unique == 0 || find_in_hash_table(buffer) == 0) {
					lines[ i ] = strdup(buffer);
					++i;
					if (unique == 1)
						add_to_hash_table(buffer);
				}
			}
		}
	}

	for(int j = i; j < MAX_LINES; ++j)
		lines[ j ] = NULL;

	return i;
}

unsigned int seed()
{
	unsigned int result = 0xdeadbeef;
	size_t s;
	FILE *f;

	f = fopen("/dev/urandom", "r");
	if(f != NULL) {
		while((s = fread(&result, sizeof(result), 1, f)) < 1);
		fclose(f);
	}

	return result;
}

void randomize_array(int array[], int size)
{
	int i, p, swap;
	for(i = 0; i < size; ++i)
		array[i] = i;
	for(i = 0; i < size; ++i) {
		p = rand() % size;
		swap = array[i];
		array[i] = array[p];
		array[p] = swap;
	}
}

int main(int argc, char *argv[])
{
	int linecount, i;
	int linelimit = INT_MAX;
	int *randarray;
	int readnull = 0;
	int quiet = 0;
	int unique = 0;
	char **lines;

	unsigned int seedvar = seed();
	for(i = 1; i < argc; ++i) {
		char *endptr = '\0';
		unsigned int numarg = 0;
		if(argv[ i ][ 0 ] == '-' && (argv[ i ][ 1 ] == '0' || argv[ i ][ 1 ] == 'Z') && argv[ i ][ 2 ] == 0)
			readnull = 1;
		else if(argv[ i ][ 0 ] == '-' && argv[ i ][ 1 ] == 'q' && argv[ i ][ 2 ] == 0)
			quiet = 1;
		else if(argv[ i ][ 0 ] == '-' && argv[ i ][ 1 ] == 'u' && argv[ i ][ 2 ] == 0)
			unique = 1;
		else if(argv[ i ][ 0 ] == '-' && argv[ i ][ 1 ] == 'n' && argv[ i ][ 2 ] == 0 && i < argc - 1) {
			++i;
			if(argv[ i ][ 0 ] >= '1' && argv[ i ][ 0 ] <= '9')
				linelimit = atoi(argv[ i ]);
		} else if(argv[ i ][ 0 ] == '-' && argv[ i ][ 1 ] == 'n' && argv[ i ][ 2 ] >= '1' && argv[ i ][ 2 ] <= '9') {
			linelimit = atoi(argv[ i ] + 2);
		} else if((argv[ i ][ 0 ] == '-' && argv[ i ][ 1 ] == 'h' && argv[ i ][ 2 ] == 0) || strcmp(argv[ i ], "--help") == 0) {
			fprintf(stderr, "Copyright (c) 2010-2014 Thomas Fischer.\nAll rights reserved.\n\n");
			fprintf(stderr, "Get the source code at:\n  https://gitorious.org/tfscripts/linuxcommandline\nReleased under the two-clause BSD license\n\n");
			fprintf(stderr, "Reads lines of text from stdin,\n");
			fprintf(stderr, "writes them out in randomized order on stdout\n\n");
			fprintf(stderr, "Usage: randline [-0] [-Z] [-q] [-l n] [seed]\n");
			fprintf(stderr, "  seed   Random seed to use; if omitted, seed is read from /dev/urandom\n");
			fprintf(stderr, "         Seed can be decimal or hexadecmial (starting with \"0x\")\n");
			fprintf(stderr, "  -0     Read and write zero-terminated lines of text\n");
			fprintf(stderr, "  -Z     Same as -0, included for compatibility\n");
			fprintf(stderr, "  -q     Quiet mode, omit verbose output\n");
			fprintf(stderr, "  -u     Keep only unique lines, i.e. no duplicates\n");
			fprintf(stderr, "  -l n   Print at most n lines\n");
			return 1;
		} else if((numarg = strtoll(argv[ i ], &endptr, 0)) > 0 && endptr[0] == '\0')
			seedvar = numarg;
	}

	if(quiet < 1) {
		fprintf(stderr, "seed=0x%08X\n", seedvar);
		if(linelimit < INT_MAX)
			fprintf(stderr, "linelimit=%d\n", linelimit);
	}
	srand(seedvar);

	if(unique > 0){
		hash_table = (struct hsearch_data *)calloc(1, sizeof(struct hsearch_data));
		hcreate_r(HASHTABSIZE, hash_table);
	}

	lines = (char**)malloc(sizeof(char*) * MAX_LINES);
	linecount = read_lines(readnull, lines, unique);
	randarray = (int*)malloc(sizeof(int) * linecount);
	randomize_array(randarray, linecount);

	for(i = 0; i < linecount && i < linelimit; i++) {
		printf("%s", lines[ randarray[i] ]);
		if(readnull)
			fputc('\0', stdout);
	}

	free(randarray);
	for(i = 0; i < linecount + 1; i++)
		free(lines[ i ]);
	free(lines);
	if(unique > 0) {
		hdestroy_r(hash_table);
		free(hash_table);
	}

	return 0;
}

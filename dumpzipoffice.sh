#!/usr/bin/env bash

# Create temporary directory
TEMPDIR=$(mktemp -d)
function cleanup {
	# Remove temporary directory
	rm -rf "${TEMPDIR}"
}
trap cleanup EXIT

# Dump an XML file including a nice header (filename and size in bytes)
function dump_xml {
	local xmlfile="$1"
	[[ -f "${xmlfile}" ]] && echo "=== ${xmlfile} ===" && echo "~~~ "$(wc -c <"${xmlfile}")" bytes ~~~" && "${XMLFORMATTER}" "${xmlfile}"
}

# Dump a plain text file including a nice header (filename and size in bytes)
function dump_plain {
	local plainfile="$1"
	[[ -f "${plainfile}" ]] && echo "=== ${plainfile} ===" && echo "~~~ "$(wc -c <"${plainfile}")" bytes ~~~" && cat "${plainfile}"
}


# Determining tool to normalize/format XML data
# Try 'xmlindent' first
XMLFORMATTER=$(which xmlindent 2>/dev/null)
# Use plain 'cat' as fallback
[[ -z "${XMLFORMATTER}" ]] && XMLFORMATTER=$(which cat 2>/dev/null)

# Extract OOXML/ODF file in temporary directory
unzip -d "${TEMPDIR}" "$1" 2>/dev/null >&2 || exit 1
# Remember current directory and go to temporary directory
ORIGDIR="${PWD}"
cd "${TEMPDIR}"

# Process general metadata files that may be found in OOXML and/or ODF files
dump_plain mimetype && echo # enforce new line
dump_xml "[Content_Types].xml"
dump_plain manifest.rdf
# Process any generic XML file found in archive
find -type f -name '*.xml' | sort | sed -e 's/^[.][/]//g' | grep -v '\[Content_Types\].xml' | while read xmlfilename ; do
	dump_xml "${xmlfilename}"
done

# Clean up afterwards
cd "${ORGIDIR}"

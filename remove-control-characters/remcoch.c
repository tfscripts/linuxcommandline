/* Copyright (c) 2017-2018, Thomas Fischer <fischer@unix-ag.uni-kl.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Thomas Fischer nor the names of other contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THOMAS FISCHER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * The purpose of this program is to read a stream of characters from stdin
 * and print it on stdout. Characters below code point 32 (so-called 'control
 * characters') will be omitted, except for the NUL, TAB, LF, and CR bytes
 * (0, 9, 10, 13).
 * The program will exit with error code 1 if it cannot write a byte to stdout.
 * Otherwise it will exit with error code 0 (no error), even if no byte was
 * read or written (such as on empty input).
 * Passing '-v' or '-V' as the single argument to this program will print the
 * copyright statement.
 */

#include <stdio.h>

int main(int argc, char *argv[])
{
	if (argc > 1 && argv[argc - 1][0] == '-' && (argv[argc - 1][1] == 'v' || argv[argc - 1][1] == 'V') && argv[argc - 1][2] == '\0') {
		fprintf(stderr, "Copyright (c) 2017-2018, Thomas Fischer <fischer@unix-ag.uni-kl.de>\n\n");
		fprintf(stderr, "Released under the so-called '3-clause BSD license'.\n");
		fprintf(stderr, "See the source code (remcoch.c) for details.\n");
		return 1;
	}

	/// Read first character
	int c = fgetc(stdin);
	/// While not at end of file ...
	while(c != EOF) {
		/// Check if previously read character is valid,
		/// i.e. regular ASCII character or allowed
		/// control character (NULL, TAB, LF, or CR)
		if(c >= 32 || c == 0 || c == 9 || c == 10 || c == 13) {
			if(fputc(c, stdout) == EOF) return 1; ///< Print valid character, but exit with error on stdout's EOF
		}
		/// Read next character
		c = fgetc(stdin);
	}
	/// Exit without error
	return 0;
}

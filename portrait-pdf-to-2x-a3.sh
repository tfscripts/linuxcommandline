#!/usr/bin/env bash

# Renders a portrait PDF (size does not really matter, can be A4 or A3)
# into two pages of landscape A3 for poster printing. The two A3 pages
# have some overlap allowing both pages to be glued together to get a
# slightly-smaller-than-A2 poster.

for inputfile in "$@" ; do
	# Normalize input file to A4 portrait PDF
	a4file="$(mktemp).pdf"
	pdfnup --outfile ${a4file} --paper a4paper --nup 1x1 --no-landscape "${inputfile}" || exit 1
	# Create first landscape A4 PDF for poster's upper part
	a3file1="$(mktemp).pdf"
	pdfnup --outfile ${a3file1} --paper a3paper --landscape --nup 1x1 --trim "0cm 14cm 0cm 0cm" "${a4file}" || exit 2
	# Create second landscape A4 PDF for poster's lower part
	a3file2="$(mktemp).pdf"
	pdfnup --outfile ${a3file2} --paper a3paper --landscape --nup 1x1 --trim "0cm 0cm 0cm 14cm" "${a4file}" || exit 3
	# Determine output filename
	if [[ "${inputfile/.pdf/}" = "${inputfile}" ]] ; then
		# Input filename does not contain ".pdf",
		# create random name instead
		outputfile="${inputfile}-${RANDOM}.pdf"
	else
		# Insert "-2xa3" into input filename's base
		outputfile="${inputfile/.pdf/-2xa3.pdf}"
	fi
	# Merge both A3 pages into one output document
	pdfjoin --outfile "${outputfile}" ${a3file1} ${a3file2} || exit 4
	# Clean up temporary files
	rm -f ${a4file} ${a3file1} ${a3file2}
done

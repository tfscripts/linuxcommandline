#!/usr/bin/perl

use strict;
use warnings;

use URI::Escape;

while (<>) {
    my @quotedurls = $_ =~ /https?(?:%3a|%25|:)[^&#?]+/gi;
    shift @quotedurls;    # skip first url
    print "\n";
    foreach my $quotedurl (@quotedurls) {
        my $unescaped = uri_unescape($quotedurl);
        $unescaped = uri_unescape($unescaped) if ($unescaped=~/%3a/i);
        print $unescaped . "\n";
    }
    print "\n";
}

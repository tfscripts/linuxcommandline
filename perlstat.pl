#!/usr/bin/env perl

use strict;
use warnings;

use Statistics::Descriptive;

if ( $#ARGV == -1 ) {
    printf STDERR "No command line arguments specified\n";
    exit 1;
}

my $stat = Statistics::Descriptive::Full->new();

while (<STDIN>) {
    my $line = $_;
    chomp($line);
    $stat->add_data($line) if ( $line =~ /^-?(\d+\.?|\d*\.\d+)$/ );
}

if ($stat->count() == 0 ) {
    printf STDERR "No valid data passed via stdin\n";
    exit 1;
}

my $separator = "\n";
my $endline   = "\n";
my $fmstr ="%.3f";

while (@ARGV) {
    my $arg = shift(@ARGV);

    if ( $arg eq "-s" ) {
        $separator = shift(@ARGV);
        $separator = "\n" if ( $separator eq "\\n" );
        next;
    }
    elsif ( $arg eq "-f" ) {
        $fmstr = shift(@ARGV);
        next;
    }
    elsif ( $arg eq "-n" ) {
        $endline = "";
        next;
    }

    if ( $arg eq "count" ) {
        printf $stat->count();
    }
    elsif ( $arg eq "sum" ) {
        printf $fmstr, $stat->sum();
    }
    elsif ( $arg eq "min" ) {
        printf $fmstr, $stat->min();
    }
    elsif ( $arg eq "max" ) {
        printf $fmstr, $stat->max();
    }
    elsif ( $arg eq "med" ) {
        printf $fmstr, $stat->median();
    }
    elsif ( $arg eq "avg" ) {
        printf $fmstr, $stat->mean();
    }
    elsif ( substr($arg,0,5) eq "quart" || substr($arg,0,5) eq "quant" ) {
        printf $fmstr, $stat->quantile(0+substr($arg,5,1));
    }
    else {
        printf STDERR "Unknown argument: " . $arg . "\n";
        exit 1;
    }

    printf $separator unless scalar(@ARGV) == 0;
}

printf $endline;
